<?php

namespace App\Tests;

use DateTime;
use App\Entity\Item;
use App\Entity\User;

use App\Entity\Categorie;
use App\Entity\Commentaire;
use PHPUnit\Framework\TestCase;

class ItemUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $item = new Item();
        $datetime = new DateTime();
        $categorie = new Categorie();
        $user = new User();

        $item->setNom('nom')
            ->setEnVente(true)
            ->setCreatedAt($datetime)
            ->setDescription('description')
            ->setSlug('slug')
            ->setFile('file')
            ->addCategorie($categorie)
            ->setPrix('20.20')
            ->setUser($user)
            ->setPortfolio(true);

        $this->assertTrue($item->getNom() === 'nom');
        $this->assertTrue($item->getEnVente() === true);
        $this->assertTrue($item->getCreatedAt() === $datetime);
        $this->assertTrue($item->getDescription() === 'description');
        $this->assertTrue($item->getSlug() === 'slug');
        $this->assertTrue($item->getFile() === 'file');
        $this->assertContains($categorie, $item->getCategorie());
        $this->assertTrue($item->getPrix() === '20.20');
        $this->assertTrue($item->getUser() === $user);
        $this->assertTrue($item->getPortfolio() === true);
    }

    public function testIsFalse()
    {
        $item = new Item();
        $datetime = new DateTime();
        $categorie = new Categorie();
        $user = new User();

        $item->setNom('nom')
            ->setEnVente(true)
            ->setCreatedAt($datetime)
            ->setDescription('description')
            ->setSlug('slug')
            ->setFile('file')
            ->addCategorie($categorie)
            ->setPrix(20.20)
            ->setUser($user)
            ->setPortfolio(true);

        $this->assertFalse($item->getNom() === 'false');
        $this->assertFalse($item->getEnVente() === false);
        $this->assertFalse($item->getCreatedAt() === new DateTime());
        $this->assertFalse($item->getDescription() === 'false');
        $this->assertFalse($item->getSlug() === 'false');
        $this->assertFalse($item->getFile() === 'false');
        $this->assertNotContains(new Categorie(), $item->getCategorie());
        $this->assertFalse($item->getPrix() === 22.20);
        $this->assertFalse($item->getUser() === new User());
    }

    public function testIsEmpty()
    {
        $item = new Item();

        $this->assertEmpty($item->getNom());
        $this->assertEmpty($item->getEnVente());
        $this->assertEmpty($item->getCreatedAt());
        $this->assertEmpty($item->getDescription());
        $this->assertEmpty($item->getSlug());
        $this->assertEmpty($item->getFile());
        $this->assertEmpty($item->getCategorie());
        $this->assertEmpty($item->getPrix());
        $this->assertEmpty($item->getUser());
        $this->assertEmpty($item->getId());
    }

    public function testAddGetRemoveCommentaire()
    {
        $item = new Item();
        $commentaire = new Commentaire();

        $this->assertEmpty($item->getCommentaires());

        $item->addCommentaire($commentaire);
        $this->assertContains($commentaire, $item->getCommentaires());

        $item->removeCommentaire($commentaire);
        $this->assertEmpty($item->getCommentaires());
    }

    public function testAddGetRemoveCategorie()
    {
        $item = new Item();
        $categorie = new Categorie();

        $this->assertEmpty($item->getCategorie());

        $item->addCategorie($categorie);
        $this->assertContains($categorie, $item->getCategorie());

        $item->removeCategorie($categorie);
        $this->assertEmpty($item->getCategorie());
    }
}
