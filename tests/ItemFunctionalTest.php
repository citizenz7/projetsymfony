<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ItemFunctionalTest extends WebTestCase
{
    public function testShouldDisplayItem(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/items');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'Items');
    }

    public function testShouldDisplayOneItem(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/items/item-test');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'item test');
    }
}
