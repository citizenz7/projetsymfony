<?php

namespace App\Tests;

use DateTime;
use App\Entity\Item;
use App\Entity\BlogPost;
use App\Entity\Commentaire;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    public function testisTrue()
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $blogpost = new BlogPost();
        $item = new Item();

        $commentaire->setAuteur('auteur')
            ->setEmail('email@test.com')
            ->setCreatedAt($datetime)
            ->setContenu('contenu')
            ->setBlogpost($blogpost)
            ->setItem($item)
        ;

        $this->assertTrue($commentaire->getAuteur() === 'auteur');
        $this->assertTrue($commentaire->getEmail() === 'email@test.com');
        $this->assertTrue($commentaire->getCreatedAt() === $datetime);
        $this->assertTrue($commentaire->getContenu() === 'contenu');
        $this->assertTrue($commentaire->getBlogpost() === $blogpost);
        $this->assertTrue($commentaire->getItem() === $item);
    }

    public function testisFalse()
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $blogpost = new BlogPost();
        $item = new Item();

        $commentaire->setAuteur('false')
            ->setEmail('false@test.com')
            ->setCreatedAt(new DateTime())
            ->setContenu('false')
            ->setBlogpost(new BlogPost())
            ->setItem(new Item())
        ;

        $this->assertFalse($commentaire->getAuteur() === 'auteur');
        $this->assertFalse($commentaire->getEmail() === 'email@test.com');
        $this->assertFalse($commentaire->getCreatedAt() === $datetime);
        $this->assertFalse($commentaire->getContenu() === 'contenu');
        $this->assertFalse($commentaire->getBlogpost() === $blogpost);
        $this->assertFalse($commentaire->getItem() === $item);
    }

    public function testisEmpty()
    {
        $commentaire = new Commentaire();

        $this->assertEmpty($commentaire->getAuteur());
        $this->assertEmpty($commentaire->getEmail());
        $this->assertEmpty($commentaire->getCreatedAt());
        $this->assertEmpty($commentaire->getContenu());
        $this->assertEmpty($commentaire->getBlogpost());
        $this->assertEmpty($commentaire->getItem());
        $this->assertEmpty($commentaire->getId());
    }

}
