<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeFunctionalTest extends WebTestCase
{
    public function testShouldDisplayHomepage(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Lorem Ipsum');
        $this->assertSelectorTextContains('h2', 'Catonem, nunc repetetur ordo gestorum socio ob aerarii nostri angustias iusso sine ulla culpa proscribi ideoque');
    }
}
