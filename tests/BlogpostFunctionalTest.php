<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BlogpostFunctionalTest extends WebTestCase
{
    public function testShouldDisplayBlogpost(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/news');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h5', 'Blogpost test');
    }

    public function testShouldDisplayOneBlogpost(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/news/blogpost-test');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'Blogpost test');
    }
}
