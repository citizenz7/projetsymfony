<?php

namespace App\Tests;

use App\Entity\BlogPost;
use App\Entity\Item;
use App\Entity\User;
Use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new User();

        $user->setEmail('true@test.com')
            ->setPrenom('prenom')
            ->setNom('nom')
            ->setPassword('password')
            ->setAPropos('a propos')
            ->setDiscord('discord')
            ->setFacebook('facebook')
            ->setTelegram('telegram')
            ->setTwitter('twitter')
            ->setRoles(['ROLE_TEST'])
            ->setIsVerified(true)
        ;

        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getUsername() === 'true@test.com');
        $this->assertTrue($user->getUserIdentifier() === 'true@test.com');
        $this->assertTrue($user->getPrenom() === 'prenom');
        $this->assertTrue($user->getNom() === 'nom');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getAPropos() === 'a propos');
        $this->assertTrue($user->getDiscord() === 'discord');
        $this->assertTrue($user->getFacebook() === 'facebook');
        $this->assertTrue($user->getTelegram() === 'telegram');
        $this->assertTrue($user->getTwitter() === 'twitter');
        $this->assertTrue($user->getRoles() === ['ROLE_TEST', 'ROLE_USER']);
        $this->assertTrue($user->isVerified() === true);
    }

    public function testIsFalse()
    {
        $user = new User();

        $user->setEmail('true@test.com')
            ->setPrenom('prenom')
            ->setNom('nom')
            ->setPassword('password')
            ->setAPropos('a propos')
            ->setDiscord('discord')
            ->setFacebook('facebook')
            ->setTelegram('telegram')
            ->setTwitter('twitter')
        ;

        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getUsername() === 'false@test.com');
        $this->assertFalse($user->getUserIdentifier() === 'false@test.com');
        $this->assertFalse($user->getPrenom() === 'false');
        $this->assertFalse($user->getNom() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getAPropos() === 'false');
        $this->assertFalse($user->getDiscord() === 'false');
        $this->assertFalse($user->getFacebook() === 'false');
        $this->assertFalse($user->getTelegram() === 'false');
        $this->assertFalse($user->getTwitter() === 'false');
    }

    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getUsername());
        $this->assertEmpty($user->getUserIdentifier());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getNom());
        //$this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getAPropos());
        $this->assertEmpty($user->getDiscord());
        $this->assertEmpty($user->getFacebook());
        $this->assertEmpty($user->getTelegram());
        $this->assertEmpty($user->getTwitter());
        $this->assertEmpty($user->getId());
    }

    public function testAddGetRemoveItem()
    {
        $user = new User();
        $item = new Item();

        $this->assertEmpty($user->getItems());

        $user->addItem($item);
        $this->assertContains($item, $user->getItems());

        $user->removeItem($item);
        $this->assertEmpty($user->getItems());
    }

    public function testAddGetRemoveBlogpost()
    {
        $user = new User();
        $blogpost = new BlogPost();

        $this->assertEmpty($user->getBlogPosts());

        $user->addBlogPost($blogpost);
        $this->assertContains($blogpost, $user->getBlogPosts());

        $user->removeBlogPost($blogpost);
        $this->assertEmpty($user->getBlogPosts());
    }
}