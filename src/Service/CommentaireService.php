<?php

namespace App\Service;

use App\Entity\BlogPost;
use App\Entity\Commentaire;
use App\Entity\Item;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class CommentaireService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistCommentaire(
        Commentaire $commentaire,
        BlogPost $blogpost = null,
        Item $item = null
    ): void {
        $commentaire->setIsPublished(false)
            ->setBlogpost($blogpost)
            ->setItem($item)
            ->setCreatedAt(new DateTime('now'));

        $this->manager->persist($commentaire);
        $this->manager->flush();

        $this->flash->add('success', 'Your comment has been sent! Thank you! It will be published once approved.');
    }
}
