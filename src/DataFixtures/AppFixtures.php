<?php

namespace App\DataFixtures;

use App\Entity\BlogPost;
use App\Entity\Categorie;
use App\Entity\Item;
use Faker\Factory;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @codeCoverageIgnore
 */
class AppFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('en_US');

        // Create 1 user
        $user = new User();

        $user->setEmail('user@test.com')
            ->setPrenom($faker->firstName())
            ->setNom($faker->lastName())
            ->setAPropos($faker->text())
            ->setFacebook('Facebook')
            ->setRoles(['ROLE_MODO']);

        $user->setPassword($this->passwordHasher->hashPassword(
            $user,
            'password'
        ));

        $manager->persist($user);

        // Create Blogposts (x10)
        for ($i = 0; $i < 10; $i++) {
            $blogpost = new BlogPost();

            $blogpost->setTitre($faker->words(3, true))
                ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                ->setContenu($faker->text(350))
                ->setSlug($faker->slug(3))
                ->setUser($user);

            $manager->persist($blogpost);
        }

        // Create Blogpost for tests
        $blogpost = new BlogPost();

        $blogpost->setTitre('Blogpost test')
            ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
            ->setContenu($faker->text(350))
            ->setSlug('blogpost-test')
            ->setUser($user);

        $manager->persist($blogpost);

        // Create 5 categories
        for ($k = 0; $k < 5; $k++) {
            $categorie = new Categorie();

            $categorie->setNom($faker->word())
                ->setDescription($faker->words(10, true))
                ->setSlug($faker->slug());

            $manager->persist($categorie);

            // Create 2 items/categorie
            for ($j = 0; $j < 3; $j++) {
                $item = new Item();

                $item->setNom($faker->words(3, true))
                    ->setEnVente($faker->randomElement([true, false]))
                    ->setPrix($faker->randomFloat(2, 100, 9999))
                    ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                    ->setDescription($faker->text())
                    ->setSlug($faker->slug())
                    ->setFile('more-services-1.jpg')
                    ->setUser($user)
                    ->addCategorie($categorie);

                $manager->persist($item);
            }
        }

        // Test categorie
        $categorie = new Categorie();

        $categorie->setNom('Item test')
            ->setDescription($faker->words(10, true))
            ->setSlug('categorie-test');

        $manager->persist($categorie);

        // Item for tests
        $item = new Item();

        $item->setNom('item test')
            ->setEnVente($faker->randomElement([true, false]))
            ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
            ->setDescription($faker->text())
            ->setPortfolio($faker->randomElement([true, false]))
            ->setSlug('item-test')
            ->setFile('more-services-1.jpg')
            ->addCategorie($categorie)
            ->setPrix($faker->randomFloat(2, 100, 9999))
            ->setUser($user);

        $manager->persist($item);

        $manager->flush();
    }
}
