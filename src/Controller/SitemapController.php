<?php

namespace App\Controller;

use App\Entity\Item;
use App\Repository\BlogPostRepository;
use App\Repository\CategorieRepository;
use App\Repository\ItemRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    /**
     * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"})
     */
    public function index(
        Request $request,
        ItemRepository $itemRepository,
        BlogPostRepository $blogPostRepository,
        CategorieRepository $categorieRepository
    ): Response {
        $hostname = $request->getSchemeAndHttpHost();

        $urls = [];

        $urls[] = ['loc'    => $this->generateUrl('home')];
        $urls[] = ['loc'    => $this->generateUrl('items')];
        $urls[] = ['loc'    => $this->generateUrl('news')];
        $urls[] = ['loc'    => $this->generateUrl('portfolio')];
        $urls[] = ['loc'    => $this->generateUrl('about')];
        $urls[] = ['loc'    => $this->generateUrl('contact')];

        foreach ($itemRepository->findAll() as $item) {
            $urls[] = [
                'loc'       => $this->generateUrl('detailitem', ['slug' => $item->getSlug()]),
                'lastmod'   => $item->getCreatedAt()->format('Y-m-d')
            ];
        }

        foreach ($blogPostRepository->findAll() as $blogPost) {
            $urls[] = [
                'loc'       => $this->generateUrl('detailnews', ['slug' => $blogPost->getSlug()]),
                'lastmod'   => $blogPost->getCreatedAt()->format('Y-m-d')
            ];
        }

        foreach ($categorieRepository->findAll() as $categorie) {
            $urls[] = [
                'loc'       => $this->generateUrl('portfolio_categorie', ['slug' => $categorie->getSlug()])
            ];
        }

        $response = new Response(
            $this->renderView('sitemap/index.html.twig', [
                'urls'      => $urls,
                'hostname'  => $hostname,
            ]),
            200
        );

        $response->headers->set('Content-type', 'text/xml');

        return $response;
    }
}
