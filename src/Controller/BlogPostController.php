<?php

namespace App\Controller;

use App\Entity\BlogPost;
use App\Entity\Commentaire;
use App\Form\CommentaireType;
use App\Repository\BlogPostRepository;
use App\Repository\CommentaireRepository;
use App\Service\CommentaireService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogPostController extends AbstractController
{
    /**
     * @Route("/news", name="news")
     */
    public function index(
        BlogPostRepository $blogpostRepository,
        Request $request,
        PaginatorInterface $paginator
    ): Response {

        //$data = $blogpostRepository->findAll();
        $data = $blogpostRepository->findBy([], ['id' => 'DESC']);

        $blogpost = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('blog_post/index.html.twig', [
            'blogposts' => $blogpost,
            'current_menu' => 'news',
        ]);
    }

    /**
     * @Route("/news/{slug}", name="detailnews")
     */
    public function detail(
        BlogPost $blogpost,
        Request $request,
        CommentaireService $commentaireService,
        CommentaireRepository $commentaireRepository
    ): Response {
        $commentaires = $commentaireRepository->findCommentaires($blogpost);
        $commentaire = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $commentaire = $form->getData();
            $commentaireService->persistCommentaire($commentaire, $blogpost, null);

            return $this->redirectToRoute('detailnews', ['slug' => $blogpost->getSlug()]);
        }

        return $this->render('blog_post/detail.html.twig', [
            'blogpost' => $blogpost,
            'commentaires' => $commentaires,
            'form' => $form->createView(),
        ]);
    }
}
