<?php

namespace App\Controller;

use App\Entity\Item;
use App\Entity\Commentaire;
use App\Form\CommentaireType;
use App\Repository\ItemRepository;
use App\Service\CommentaireService;
use App\Repository\CommentaireRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ItemController extends AbstractController
{
    /**
     * @Route("/items", name="items")
     */
    public function index(
        ItemRepository $itemRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $itemRepository->findBy([], ['id' => 'DESC']);

        $items = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('item/index.html.twig', [
            'items' => $items,
            'current_menu' => 'items',
        ]);
    }

    /**
     * @Route("/items/{slug}", name="detailitem")
     */
    public function detail(
        Item $item,
        Request $request,
        CommentaireService $commentaireService,
        CommentaireRepository $commentaireRepository
    ): Response {
        $commentaires = $commentaireRepository->findCommentaires($item);
        $commentaire = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $commentaire = $form->getData();
            $commentaireService->persistCommentaire($commentaire, null, $item);

            return $this->redirectToRoute('detailitem', ['slug' => $item->getSlug()]);
        }

        return $this->render('item/detail.html.twig', [
            'item' => $item,
            'form' => $form->createView(),
            'commentaires' => $commentaires,
        ]);
    }
}
