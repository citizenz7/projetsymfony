<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            EmailField::new('email'),
            TextField::new('prenom'),
            TextField::new('nom'),
            TextField::new('adresse'),
            TextField::new('telephone'),
            TextareaField::new('description')->onlyOnForms(),
            TextareaField::new('a_propos')->onlyOnForms(),
            TextField::new('discord')->onlyOnForms(),
            TextField::new('telegram')->onlyOnForms(),
            TextField::new('facebook')->onlyOnForms(),
            TextField::new('twitter')->onlyOnForms(),
            TextField::new('github')->onlyOnForms(),
            TextField::new('linkedin')->onlyOnForms(),
            BooleanField::new('is_verified')->onlyOnIndex(),
        ];
    }

    // On modifie les différentes fonctions sur EasyAdmin
    // car on ne veut pas laisser la possibilité
    // de SUPPRIMER ou de CREER un nouveau user
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // On veut un bouton qui nous permette d'accéder aux Détails d'un user
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            // On DESACTIVE le bouton DELETE et le bouton NEW
            ->disable(Action::DELETE, Action::NEW);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'About')
            ->setPageTitle('edit', 'Edit about')
            ->setDefaultSort(['id' => 'DESC']);
    }
}
