<?php

namespace App\Controller\Admin;

use App\Entity\BlogPost;
use App\Entity\Categorie;
use App\Entity\Commentaire;
use App\Entity\Item;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Lorem Ipsum');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::section('MAIN');
        yield MenuItem::linkToRoute('Home', 'fas fa-home', 'home');
        yield MenuItem::section('ADMIN');
        yield MenuItem::linktoDashboard('Dashboard', 'fas fa-users-cog');
        yield MenuItem::linkToCrud('Categories', 'fas fa-tags', Categorie::class);
        yield MenuItem::linkToCrud('News', 'fas fa-newspaper', BlogPost::class);
        yield MenuItem::linkToCrud('Items', 'fas fa-palette', Item::class);
        yield MenuItem::linkToCrud('Comments', 'fas fa-comment', Commentaire::class);
        yield MenuItem::linkToCrud('About', 'fas fa-user', User::class);
    }
}
