<?php

namespace App\Controller;

use App\Repository\BlogPostRepository;
use App\Repository\ItemRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(
        ItemRepository $ItemRepository,
        BlogPostRepository $BlogPostRepository,
        UserRepository $userRepository
    ): Response {
        return $this->render('home/index.html.twig', [
            'items' => $ItemRepository->lasItems(),
            'blogposts' => $BlogPostRepository->lastPost(),
            'modo' => $userRepository->getModo(),
        ]);
    }
}
